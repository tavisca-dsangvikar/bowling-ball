﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling
{
    public class Game
    {
        int[] _rolls= new int[30];
        int _rollCount = 0;

       
        public void Roll(int pins)
        {
           _rolls[_rollCount++] = pins;

           if (pins == 10)
               _rollCount++;
        }

        public int GetScore()
        {
            int score = 0;
            for (int i = 0; i < 20 ; i++)
            {
                score += _rolls[i];
                if (_rolls[i] == 10 && _rolls[i+2] == 10 && (i % 2 == 0))
                    score += _rolls[i + 2] + _rolls[i + 4];
                else if (_rolls[i] == 10 && (i % 2 == 0))
                    score += _rolls[i + 2] + _rolls[i+3];
                else if (((_rolls[i] + _rolls[i+1]) == 10) && (i % 2==0))
                    score += _rolls[i + 2];
            }

            

            return score;
        }

    }
}
